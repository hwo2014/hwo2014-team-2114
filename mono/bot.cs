using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections;

namespace hwo
{

    public class Bot
    {
        private StreamWriter writer;
        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];
            string trackName = string.Empty;
            string carCount = string.Empty;
            string joinStr = "{\"msgType\": \"joinRace\", \"data\": {\"botId\": {\"name\": \"" + botName + "\",\"key\": \"" + botKey + "\"}";
            if (args.Length > 5)
            {
                trackName = args[4];
                carCount = args[5];
                joinStr += ",\"trackName\": \"" + trackName + "\"\"carCount\": " + carCount + "}";
            }
            joinStr += "}";
            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
            try
            {
                using (TcpClient client = new TcpClient(host, port))
                {
                    NetworkStream stream = client.GetStream();
                    StreamReader reader = new StreamReader(stream);
                    StreamWriter writer = new StreamWriter(stream);
                    writer.AutoFlush = true;
                    new Bot(reader, writer, botName, botKey, trackName, carCount);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        Bot(StreamReader reader, StreamWriter writer, string botName, string botKey, string trackName, string carCount)
        {
            string joinStr = "{\"msgType\": \"join\", \"data\": {\"name\": \"" + botName + "\",\"key\": \"" + botKey + "\"}}";
            if (!string.IsNullOrEmpty(trackName) && !string.IsNullOrEmpty(carCount))
                joinStr = "{\"msgType\": \"joinRace\", \"data\": {\"botId\": {\"name\": \"" + botName + "\",\"key\": \"" + botKey + "\"}" + ",\"trackName\": \"" + trackName + "\" " + ",\"carCount\":"+carCount+"}}";

            
            RaceAnalyzer raceAnalyzer = new RaceAnalyzer(botName,"red");
            raceAnalyzer.coefFriction = .45;
            raceAnalyzer.outputToConsole = false;
            raceAnalyzer.CI = true;
            this.writer = writer;
            string line;
            //Console.WriteLine(joinStr);
            writer.WriteLine(joinStr);
            bool quit = false;
            string sPing = "{\"msgType\": \"ping\"}";
            while ((line = reader.ReadLine()) != null && !quit)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "yourCar":
                        {
                            raceAnalyzer.IdentifyCar(JsonConvert.DeserializeObject<YourCarMsg.RootObject>(line));
                            writer.WriteLine(sPing);
                            break;
                        }
                    case "turboAvailable":
                        {
                            raceAnalyzer.UpdateTurboStatus(JsonConvert.DeserializeObject<TurboMsg.RootObject>(line).data);
                            writer.WriteLine(sPing);
                            break;
                        }
                    case "crash":
                        writer.WriteLine(sPing);
                        break;
                    case "spawn":
                        writer.WriteLine(sPing);
                        break;
                    case "carPositions":
                        {
                            raceAnalyzer.UpdateCarPos(JsonConvert.DeserializeObject<CarPositionsMessage.RootObject>(line));
                            string response = sPing;
                            if (raceAnalyzer.gameTick != 0)
                            {
                                if (raceAnalyzer.switchLanes)
                                    response = "{\"msgType\": \"switchLane\", \"data\":\"" + raceAnalyzer.switchCmd + "\", \"gameTick\":" + raceAnalyzer.gameTick.ToString() + " }";
                                else if (raceAnalyzer.isTurboTime)
                                    response = "{\"msgType\":\"turbo\", \"data\":\"" + raceAnalyzer.EngageTurbo() +"\", \"gameTick\":" + raceAnalyzer.gameTick.ToString() + " }";
                                else
                                    response = "{\"msgType\":\"throttle\", \"data\":" + raceAnalyzer.throttleOutput + ", \"gameTick\":" + raceAnalyzer.gameTick.ToString() + " }";
                            }
                            writer.WriteLine(response);
                        }
                        break;
                    case "join":
                        writer.WriteLine(sPing);
                        break;
                    case "gameInit":
                        raceAnalyzer.InitRace(JsonConvert.DeserializeObject<GameInitMsg.RootObject>(line));
                        writer.WriteLine(sPing);
                        break;
                    case "gameEnd":
                        raceAnalyzer.GameEnd();
                        writer.WriteLine(sPing);
                        break;
                    case "gameStart":
                        raceAnalyzer.GameStart();
                        writer.WriteLine(sPing);
                        break;
                    default:
                        writer.WriteLine(sPing);
                        break;
                }
            }
            raceAnalyzer.dumpAccelerationStats();
            raceAnalyzer.dumpDecelerationsStats();
            raceAnalyzer.dumpPieceStats();
            raceAnalyzer.dumpThrottleStats();
        }
    }


    class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
    public class YourCarMsg
    {
        public class Data
        {
            public string name { get; set; }
            public string color { get; set; }
        }

        public class RootObject
        {
            public string msgType { get; set; }
            public Data data { get; set; }
        }
    }
    public class TurboMsg
    {
        public class Data
        {
            public double turboDurationMilliseconds { get; set; }
            public int turboDurationTicks { get; set; }
            public double turboFactor { get; set; }
        }

        public class RootObject
        {
            public string msgType { get; set; }
            public Data data { get; set; }
        }
    }
    public class CarPositionsMessage
    {
        public class Id
        {
            public string name { get; set; }
            public string color { get; set; }
        }

        public class Lane
        {
            public int startLaneIndex { get; set; }
            public int endLaneIndex { get; set; }
        }

        public class PiecePosition
        {
            public int pieceIndex { get; set; }
            public double inPieceDistance { get; set; }
            public Lane lane { get; set; }
            public int lap { get; set; }
        }

        public class Datum
        {
            public Id id { get; set; }
            public double angle { get; set; }
            public PiecePosition piecePosition { get; set; }
        }

        public class RootObject
        {
            public string msgType { get; set; }
            public List<Datum> data { get; set; }
            public string gameId { get; set; }
            public int gameTick { get; set; }
        }
    }
    public class GameInitMsg
    {
        public class Piece
        {
            public double length { get; set; }
            public bool? @switch { get; set; }
            public int? radius { get; set; }
            public double? angle { get; set; }
        }

        public class Lane
        {
            public int distanceFromCenter { get; set; }
            public int index { get; set; }
        }

        public class Position
        {
            public double x { get; set; }
            public double y { get; set; }
        }

        public class StartingPoint
        {
            public Position position { get; set; }
            public double angle { get; set; }
        }

        public class Track
        {
            public string id { get; set; }
            public string name { get; set; }
            public List<Piece> pieces { get; set; }
            public List<Lane> lanes { get; set; }
            public StartingPoint startingPoint { get; set; }
        }

        public class Id
        {
            public string name { get; set; }
            public string color { get; set; }
        }

        public class Dimensions
        {
            public double length { get; set; }
            public double width { get; set; }
            public double guideFlagPosition { get; set; }
        }

        public class Car
        {
            public Id id { get; set; }
            public Dimensions dimensions { get; set; }
        }

        public class RaceSession
        {
            public int laps { get; set; }
            public int maxLapTimeMs { get; set; }
            public bool quickRace { get; set; }
        }

        public class Race
        {
            public Track track { get; set; }
            public List<Car> cars { get; set; }
            public RaceSession raceSession { get; set; }
        }

        public class Data
        {
            public Race race { get; set; }
        }

        public class RootObject
        {
            public string msgType { get; set; }
            public Data data { get; set; }
            public string gameId { get; set; }
        }
    }
 
}