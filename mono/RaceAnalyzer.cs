﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections;
using hwo;

namespace hwo
{
    class RaceAnalyzer
    {
        private bool gameStarted = false;
        public string fileTime = DateTime.Now.TimeOfDay.Hours.ToString() + "_" + DateTime.Now.TimeOfDay.Minutes.ToString();
        public bool CI = false;
        public bool outputToConsole = true;
        public double coefFriction = .45;
        private DecelerationTable decelerationTable = new DecelerationTable();
        private DecelerationTable2 decelerationTable2 = new DecelerationTable2();
        private AccelerationTable2 accelerationTable2 = new AccelerationTable2();  //currently not used
        private GameInitMsg.Race race;
        private string gameID;
        private List<CarPositionsMessage.Datum> carPositions = new List<CarPositionsMessage.Datum>();
        private List<int> carPositionsTicks = new List<int>();
        public bool turboInitialized = false;
        public bool isTurboTime { get { return (turboInitialized && currPieceIndex == maxContStraightIdx); } }
        public TurboMsg.Data turboData;
        public int turboStartPieceIndx = -1;
        public string carName;
        public string carColor;
        private int numPieces { get { return race.track.pieces.Count; } }
        private double CurrPieceAngle = 0;
        private int currPieceRadius = 0;
        private double currPieceLength = 0;
        private double currSlipAngle = 0;
        private bool isSlipping { get { return (Math.Abs(this.currSlipAngle) > Math.Abs(this.currSlipAngle)); } }
        private double degOfSlip 
        { 
            get 
            { 
                if (carPositions.Count < 2) 
                { return 0; } 
                else 
                {
                    if (currSlipAngle > 0 && carPositions[carPositions.Count - 2].angle> 0 )
                        return this.currSlipAngle - carPositions[carPositions.Count - 2].angle;
                    else if (currSlipAngle < 0 && carPositions[carPositions.Count - 2].angle < 0)
                        return (this.currSlipAngle * -1) - (carPositions[carPositions.Count - 2].angle * -1);
                    else
                        return -1; 
                } 
            } 
        }
        public int gameTick = 0;
        private int currLap = 0;
        private int prevPieceIndex { get { if ((currPieceIndex - 1) < 0) return race.track.pieces.Count - 1; else return currPieceIndex - 1; } }
        private int currPieceIndex = 0;
        public int nextPieceIndex { get { if ((currPieceIndex + 1) > (race.track.pieces.Count - 1)) return 0; else return currPieceIndex + 1; } }
        private int currLaneAdjustmentDist = 0;
        private double distanceToNextPiece = 0;
        private double currThrottle = 0;
        private string currThrottleState = "";
        private double currVelocity = 0;
        private bool atCurrTargetVelocity = false;
        private double lastTargetVelocity = 0;
        private double currAcceleration = 0;
        private List<ThrottleStatistic> throttleStats = new List<ThrottleStatistic>();
        private SortedDictionary<int, PieceStatistic> pieceStats = new SortedDictionary<int, PieceStatistic>();
        public Dictionary<int, string> switches = new Dictionary<int, string>();
        public SortedDictionary<int, CurvePieceStat> curvePieceStats = new SortedDictionary<int, CurvePieceStat>();
        public Dictionary<int, double> continuousStraights = new Dictionary<int, double>();
        public int maxContStraightIdx = 0;
        private int nextCurveIndex = 0;
        private double distanceToNextCurve = 0;
        GameInitMsg.Piece nextCurve;
        public double throttleOutput { get { return currThrottle; } }
        private int lastSwitchPiece = -1;
        public string switchCmd = "Right";
        public bool switchLanes
        {
            get
            {
                if (switches.ContainsKey(nextPieceIndex) && lastSwitchPiece != nextPieceIndex)
                {
                    lastSwitchPiece = nextPieceIndex;
                    switchCmd = switches[nextPieceIndex];
                    return true;
                }
                else
                    return false;
            }
        }
        private bool coefFrictionDeduced = false;
        private void DeduceCoefFriction(double FirstAcceleration)
        {
            //curve slip angle physics seems to be more complex than just a coef of friction, but dont have time to work out alternatives options so we are going with taht model for determining cornering speeds.
            //here we try to raise or lower the coeficient of friction based on how the first accleration stat differs from what we saw in contant physics test server
            //when starting at throttle 1 from a stopped state the first measured accleleration was: 0.196 this we conservatively estimate to a .45 coef factor.
            coefFriction = (((.196 - FirstAcceleration) / .196) * .45) + .45;    
            //coefFriction -= .01; //calculations occasioanlly a bit (about .025) higher than should be so dropping a bit to be safe
            coefFrictionDeduced = true;
        }
        public RaceAnalyzer(string Name, string Color)
        {
            carName = Name;
            carColor = Color;
        }
        public void InitRace(GameInitMsg.RootObject InitMsg)
        {
            race = InitMsg.data.race;
            gameID = InitMsg.gameId;
            turboInitialized = false;
            if (curvePieceStats.Count == 0)
                EnumerateCurvePieces();
            if (switches.Count == 0)
                AnalyzeSwitchPieces();
            RecalculateCurveMetrics();
        }
        public void GameStart()
        {
            gameStarted = true;
            carPositions.Clear();
            carPositionsTicks.Clear();
            currAcceleration = 0;
            currVelocity = 0;
            CurrPieceAngle = 0;
            currPieceRadius = 0;
            currLap = -1;
        }
        public void GameEnd()
        {
            gameStarted = false;
            carPositions.Clear();
            carPositionsTicks.Clear();
            currAcceleration = 0;
            currVelocity = 0;
            CurrPieceAngle = 0;
            currPieceRadius = 0;
            currLap = -1;
        }
        public void UpdateCarPos(CarPositionsMessage.RootObject CarPosMsg)
        {
            gameTick = CarPosMsg.gameTick;
            if (gameStarted)
            {
                for (int i = 0; i < CarPosMsg.data.Count; i++)
                {
                    if (CarPosMsg.data[i].id.color == carColor)
                    {
                        //save previous states in temporary variables for statistic calculations
                        double prevAcceleration = currAcceleration;
                        double prevVelocity = currVelocity;
                        double prevPosAngle = CurrPieceAngle;
                        double prevPosRadius = currPieceRadius;
                        int prevLap = currLap;
                        //////
                        int ticksElapsed = 0;
                        if (carPositionsTicks.Count > 0)
                            ticksElapsed = CarPosMsg.gameTick - carPositionsTicks[carPositionsTicks.Count - 1];
                        carPositions.Add(CarPosMsg.data[i]);
                        carPositionsTicks.Add(CarPosMsg.gameTick);
                        currLap = CarPosMsg.data[i].piecePosition.lap;
                        currPieceIndex = CarPosMsg.data[i].piecePosition.pieceIndex;
                        CurrPieceAngle = GetValue(race.track.pieces[currPieceIndex].angle, 0);
                        currPieceRadius = GetValue(race.track.pieces[currPieceIndex].radius, 0);
                        currPieceLength = race.track.pieces[currPieceIndex].length;
                        currSlipAngle = CarPosMsg.data[i].angle;
                        if (currSlipAngle > 58 || currSlipAngle < -58)
                            coefFriction -= .005;
                        int sign = 1;
                        if (CurrPieceAngle > 0)
                            sign = -1;
                        currLaneAdjustmentDist = sign * (race.track.lanes[CarPosMsg.data[i].piecePosition.lane.startLaneIndex].distanceFromCenter);
                        //distance to next piece may be off if calculated during a lane switch. will need to work on that
                        distanceToNextPiece = race.track.pieces[CarPosMsg.data[i].piecePosition.pieceIndex].length - CarPosMsg.data[i].piecePosition.inPieceDistance;
                        if (currPieceRadius > 0)
                            distanceToNextPiece = (((double)Math.Abs(CurrPieceAngle) / (double)360) * (double)2 * Math.PI * (double)(currPieceRadius + currLaneAdjustmentDist)) - CarPosMsg.data[i].piecePosition.inPieceDistance;
                        UpdateDistancesToCurvePieces();
                        if (prevLap > -1 && currLap > prevLap)
                            AdjustCurveStatsFromLapData();
                        RecalculateCurveMetrics();
                        double distance = EstimateVelocityAndAcceleration(ticksElapsed);
                        if (currThrottle == 0 && prevVelocity > 0)
                            decelerationTable2.Add(prevVelocity, Math.Abs(currAcceleration));
                        //if (myCurrentThrottle == 1 && prevVelocity > 0)
                        //    accelerationTable2.Add(prevVelocity, Math.Abs(myCurrentAcceleration));
                        if (currThrottle == 1 && prevVelocity > 0 && !coefFrictionDeduced)
                            DeduceCoefFriction(currAcceleration);
                        if (pieceStats.ContainsKey(currPieceIndex))
                            pieceStats[currPieceIndex].Update(currVelocity, currSlipAngle);
                        else
                            pieceStats.Add(currPieceIndex, new PieceStatistic(currVelocity, currSlipAngle));
                        if (distance > 0)
                            throttleStats.Add(new ThrottleStatistic(prevVelocity, currThrottle, currVelocity, ticksElapsed, distance, currThrottleState, prevPosAngle, prevPosRadius));
                        //SetConstantThrottle(.4);
                        AdjustThrottle();
                    }
                }
            }

        }
        private double AdjustThrottle()
        {
            string state = "";
            int breakingDistance = 0;
            double desiredVelocity = 10;
            double throttle = 1;
            if (curvePieceStats.ContainsKey(currPieceIndex))
            {
                desiredVelocity = curvePieceStats[currPieceIndex].maxCorneringSpeed;
                breakingDistance = (int)curvePieceStats[currPieceIndex].currentBreakingDistance;
            }
            
            if (distanceToNextCurve < breakingDistance || currPieceRadius > 0)
            {
                if (currPieceRadius > 99 && GetValue(race.track.pieces[nextPieceIndex].radius, 0) == 0 && distanceToNextPiece > 0 && currSlipAngle > -55.5 && currSlipAngle < 55.5)
                {
                    desiredVelocity = 10;
                    state = "Tail end piece of curve; next piece straight; floor it!";
                }
            }
            foreach (KeyValuePair<int,CurvePieceStat> kv in curvePieceStats)
                if (kv.Value.distanceTo < kv.Value.currentBreakingDistance)
                {
                    if (desiredVelocity > kv.Value.maxCorneringSpeed)
                    {
                        desiredVelocity = kv.Value.maxCorneringSpeed;
                        breakingDistance = (int)kv.Value.currentBreakingDistance;
                        //if (myCurrentLap > 0 && myPieceStats[kv.Key].maxSlipAngle < 10 && myPieceStats[kv.Key].maxSlipAngle > -10)  //if we have a relativeilty safe piece in history shorten the breaking distance.
                        //    breakingDistance = breakingDistance - 10;
                    }
                }
            double minV = desiredVelocity - .025; //min value to be "at target velocity"
            double maxV = desiredVelocity + .025; //max value to be "at target velocity"
            /*if (desiredVelocity != lastTargetVelocity)// check if "target" velocity has changed
            {
                lastTargetVelocity = desiredVelocity; //if so update target 
                atCurrTargetVelocity = false;           //and record that we are no longer within target range
            }*/
            if (currVelocity < maxV && currVelocity > minV)
                atCurrTargetVelocity = true;
            else
                atCurrTargetVelocity = false;
            if (atCurrTargetVelocity)
            {
                throttle = desiredVelocity / 10;
                state = "maintaining " + desiredVelocity.ToString();
            }
            else if (currVelocity > desiredVelocity)
            {
                double speedDiff = currVelocity - desiredVelocity;
                state = "Hard Slowdown to " + desiredVelocity.ToString();
                throttle = 0;
            }
            else
            {
                double speedDiff = (desiredVelocity - currVelocity);
                state = "Hard Speedup to " + desiredVelocity.ToString();
                throttle = 1.0;
            }
            
            if (state == "Hard Slowdown to 10")
            {
                state = "BOOOST oddity";
                throttle = 1.0;
            }
            
            if (degOfSlip > 3.5 || currSlipAngle > 55 || currSlipAngle < -55)
                throttle = 0;

            if (throttle >= 1)
                throttle = 1;
            if (throttle < 0)
                throttle = 0;

            currThrottle = throttle;
            currThrottleState = state;
            LogToFile(breakingDistance, desiredVelocity);
            return throttle;
        }
        private double SetConstantThrottle(double throttle)
        {
            double idealBankingSpeed = 0;
            int breakingDistance = 0;
            idealBankingSpeed = GetMaxBankingSpeed_v1();
            breakingDistance = (int)decelerationTable.GetBreakingDistance(currVelocity,idealBankingSpeed);
            //myCurrentThrottle = throttle;
            if (currAcceleration < .1 && Math.Abs(currSlipAngle) < 55)
                currThrottle = currThrottle + .1;
            else
                currThrottle = 0;
            //Console.WriteLine(myCurrPieceIndex +" " + EstimateCoefficientOfFriction());
            currThrottleState = "constant";
            LogToFile(breakingDistance, idealBankingSpeed);
            return throttle;
        }
        /// <summary>
        /// Returns the distance traveled.  If distance travelled calculation occures over a curve we use last estimates for velocity and acceleration and fuction will return -1
        /// </summary>
        /// <returns></returns>
        private double EstimateVelocityAndAcceleration(int ticksElapsed)
        {
            double result = -1;
            if (carPositions.Count > 1)
            {
                double pos_diff = 0;
                bool useLastEstimate = false;
                if (carPositions[carPositions.Count - 2].piecePosition.pieceIndex == carPositions[carPositions.Count - 1].piecePosition.pieceIndex) //need to accomodate speed calculation when crossing pieces.
                    pos_diff = carPositions[carPositions.Count - 1].piecePosition.inPieceDistance - carPositions[carPositions.Count - 2].piecePosition.inPieceDistance;
                else
                {
                    if (race.track.pieces[prevPieceIndex].length > 0 && currPieceLength > 0) //only do this for piece transitions that do not involve curves
                    {
                        //if (lastPieceLength == 0)
                        //    lastPieceLength = (((double)Math.Abs(currPieceAngle) / (double)360) * (double)2 * Math.PI * (double)(currPieceRadius + lastLaneAdjustmentDistance));
                        pos_diff = race.track.pieces[prevPieceIndex].length - carPositions[carPositions.Count - 2].piecePosition.inPieceDistance;
                        if (pos_diff < 0) //if last position recording along last piece had exceeded its length
                            pos_diff = 0;
                        int k = prevPieceIndex;
                        while (k != currPieceIndex && k != prevPieceIndex) //in practice this code should never get executed;
                        {
                            k++;
                            if (k > race.track.pieces.Count - 1)
                                k = 0;
                            if (race.track.pieces[k].length > 0)
                                pos_diff += race.track.pieces[k].length; //this wont work for curved pieces since their length is 0 
                            else//if we have a curve, determing its length using whatever the last laneadjutmet was (may not be accurrate for entire length is we switched lanes) 
                                useLastEstimate = true; //pos_diff +=(((double)Math.Abs(currPieceAngle) / (double)360) * (double)2 * Math.PI * (double)(currPieceRadius + lastLaneAdjustmentDistance));
                        }
                        pos_diff += carPositions[carPositions.Count - 1].piecePosition.inPieceDistance;
                    }
                    else
                        useLastEstimate = true;
                    //if piece transition involves a curve we currently do nothing and system will use last estimates for acceleration and speed
                    //due to complexity in calculatin the distance traveled in the last piece I am ignoring during a piece crossover I am ignoring it for now and using previous estimates for speed.
                }
                if (!useLastEstimate)
                {
                    double prevVelocity = currVelocity;
                    currVelocity = pos_diff / (double)ticksElapsed;
                    currAcceleration = currVelocity - prevVelocity;
                    result = pos_diff;
                }
                else
                    result = -1;

            }
            else
            {
                currVelocity = 0;
                currAcceleration = 0;
                result = 0;
            }
            return result;
        }
        public void AdjustCurveStatsFromLapData()
        {
            for (int i = 0; i< race.track.pieces.Count; i++)
            {
                int nextPiece = i+1;
                if (nextPiece > race.track.pieces.Count)
                    nextPiece = 0;
                if (race.track.pieces[i].radius > 0)
                {
                    double slipDiff = 0;
                        if (pieceStats[nextPiece].maxSlipAngle > 0 && pieceStats[nextPiece].maxSlipAngle < pieceStats[i].maxSlipAngle)
                            slipDiff = pieceStats[i].maxSlipAngle - pieceStats[nextPiece].maxSlipAngle;
                        else if (pieceStats[i].maxSlipAngle < 0)
                            if (pieceStats[nextPiece].maxSlipAngle < 0 && pieceStats[nextPiece].maxSlipAngle > pieceStats[i].maxSlipAngle)
                                slipDiff = Math.Abs(pieceStats[i].maxSlipAngle - pieceStats[nextPiece].maxSlipAngle);

                    
                    if (pieceStats[i].maxSlipAngle < 55 && pieceStats[i].maxSlipAngle > -55)
                        {
                            if (slipDiff > 30)
                                curvePieceStats[i].corneringSpeedModifier += 2;
                            else if (slipDiff > 20)
                                curvePieceStats[i].corneringSpeedModifier += 1;
                            if (slipDiff > 10)
                                curvePieceStats[i].corneringSpeedModifier += .5;
                            else if (slipDiff > 1 && pieceStats[i].maxSlipAngle < 10 && pieceStats[i].maxSlipAngle > -10)
                                curvePieceStats[i].corneringSpeedModifier += 1.0;
                            else if (pieceStats[i].maxSlipAngle < 10 && pieceStats[i].maxSlipAngle > -10)
                                curvePieceStats[i].corneringSpeedModifier += .25;
                        }
                }
            }
            
        }
        public double EstimateCoefficientOfFriction()
        {
            int idx = nextCurveIndex;
            double savedCF = coefFriction;
            if (pieceStats.ContainsKey(idx))
            {
                double r = nextCurve.radius.Value;
                //if (myPieceStats[idx].maxSlipAngle < 13)
                double result = coefFriction;
                if (Math.Abs(currSlipAngle) > 55)
                {
                    result = Math.Pow(currVelocity, 2) / currPieceRadius;
                }
            }
            return savedCF;
        }
        private void UpdateDistancesToCurvePieces()
        {
            bool curveFound = false;
            int k = currPieceIndex;
            distanceToNextCurve = distanceToNextPiece;
            while (!curveFound)
            {
                k++;
                if (k > race.track.pieces.Count - 1)
                    k = 0;
                if (race.track.pieces[k].radius > 0)
                {
                    nextCurveIndex = k;
                    nextCurve = race.track.pieces[k];
                    curveFound = true;
                }
                else
                    distanceToNextCurve += race.track.pieces[k].length;
            }

        }
        private void EnumerateCurvePieces()
        {
            curvePieceStats.Clear();
            for (int i = 0; i< race.track.pieces.Count; i++)
            {
                if (race.track.pieces[i].radius > 0)
                {
                    CurvePieceStat c = new CurvePieceStat(GetValue(race.track.pieces[i].angle,0), GetValue(race.track.pieces[i].radius,0));
                    curvePieceStats.Add(i, c);
                }
            }
        }
        private void RecalculateCurveMetrics()
        {
            int distFromCenter = 0;
            double cumulativeDistance = distanceToNextPiece;
            int i = currPieceIndex + 1;
            if (i > race.track.pieces.Count -1)
                i = 0;
            while (i != currPieceIndex)
            {
                if (curvePieceStats.ContainsKey(i))
                {
                    curvePieceStats[i].distanceTo = cumulativeDistance;
                    int sign = 1;
                    if (curvePieceStats[i].angle > 0)
                        sign = -1;
                    if (carPositions.Count != 0)
                        distFromCenter = race.track.lanes[carPositions[carPositions.Count - 1].piecePosition.lane.startLaneIndex].distanceFromCenter;
                    int thisPieceLaneAdjustmentDist = sign * (distFromCenter);
                    curvePieceStats[i].maxCorneringSpeed = Math.Sqrt((double)(curvePieceStats[i].radius + thisPieceLaneAdjustmentDist) * coefFriction) + curvePieceStats[i].corneringSpeedModifier;
                    if (decelerationTable2.isReady)
                        curvePieceStats[i].currentBreakingDistance = (int)decelerationTable2.GetBreakingDistance(currVelocity, curvePieceStats[i].maxCorneringSpeed);
                    else
                        curvePieceStats[i].currentBreakingDistance = (int)decelerationTable.GetBreakingDistance(currVelocity, curvePieceStats[i].maxCorneringSpeed);
                    cumulativeDistance += (((double)Math.Abs(curvePieceStats[i].angle) / (double)360) * (double)2 * Math.PI * (double)(curvePieceStats[i].radius + thisPieceLaneAdjustmentDist));
                }
                else //if (race.track.pieces[i].radius == null || race.track.pieces[i].radius == 0)
                    cumulativeDistance += race.track.pieces[i].length;
                i++;
                if (i > race.track.pieces.Count - 1)
                    i = 0;
            }
        }
        public void AnalyzeSwitchPieces()
        {
            switches.Clear();
            //bool inMultiPieceCurve = false; 
            for (int i = 0; i<race.track.pieces.Count; i++)
            {
                int nextIdx = i+1;
                if (nextIdx >= race.track.pieces.Count)
                    nextIdx = 0;
                if (GetValue(race.track.pieces[i].@switch,false) == true)
                {
                    int k = i + 1;
                    double rightCurveDist = 0;
                    double leftCurvDist = 0; 
                    while (k < race.track.pieces.Count && GetValue(race.track.pieces[k].@switch, false) == false)
                    {
                        double angle = (GetValue(race.track.pieces[k].angle, 0));
                        if (angle > 0)
                            rightCurveDist += ((double)Math.Abs(angle) / (double)360) * (double)2 * Math.PI * (double)(race.track.pieces[k].radius);
                        else if (angle < 0)
                            leftCurvDist += ((double)Math.Abs(angle) / (double)360) * (double)2 * Math.PI * (double)(race.track.pieces[k].radius);
                        k++;
                    }
                    if (rightCurveDist > leftCurvDist)
                        switches.Add(i, "Right");
                    else if (leftCurvDist > rightCurveDist)
                        switches.Add(i, "Left");
                }
            }
        }
        public void UpdateTurboStatus(TurboMsg.Data TurboData)
        {
            turboInitialized = true;
            turboData = TurboData;
            if (continuousStraights.Count == 0)
            {
                //calculate best place to use turbo: (begining of longest straightaway)
                int idx = nextCurveIndex;
                int StartIdx = 0;
                bool inStraight = false;
                int endIdx = nextCurveIndex - 1;
                if (endIdx == -1)
                    endIdx = race.track.pieces.Count - 1;
                double straightRunLength = 0;
                maxContStraightIdx = 0;
                continuousStraights.Clear();
                while (idx != endIdx)
                {
                    idx++;
                    if (idx >= race.track.pieces.Count)
                        idx = 0;
                    if (race.track.pieces[idx].length > 0)
                    {
                        if (inStraight == false)
                        {
                            StartIdx = idx;
                            inStraight = true;
                        }
                        straightRunLength += race.track.pieces[idx].length;
                    }
                    else if (inStraight == true)
                    {
                        continuousStraights.Add(StartIdx, straightRunLength);
                        if (!continuousStraights.ContainsKey(maxContStraightIdx))
                            maxContStraightIdx = StartIdx;
                        else if (continuousStraights[StartIdx] >= continuousStraights[maxContStraightIdx])
                            maxContStraightIdx = StartIdx;
                        straightRunLength = 0;
                        inStraight = false;
                    }
                }
            }
        }
        public void IdentifyCar(YourCarMsg.RootObject yco)
        {
            carName = yco.data.name;
            carColor = yco.data.color;
        }
        public string EngageTurbo()
        {
            turboInitialized = false;
            return "WOWOWOW";
        }
        /// <summary>
        /// Calculates the maximum cornering speed
        /// See: http://stevemunden.com/leanangle.html for more info.
        /// </summary>
        /// <returns></returns>
        private double GetMaxBankingSpeed_v1()
        {
            if (currPieceRadius > 0 || currPieceRadius > GetValue(race.track.pieces[nextPieceIndex].radius, 0))
                return Math.Sqrt((double)(currPieceRadius + currLaneAdjustmentDist) * coefFriction);
            else
                return Math.Sqrt((double)(GetValue(nextCurve.radius.Value, 0) + currLaneAdjustmentDist) * coefFriction);
        }
        private bool GetValue(bool? value, bool nullValue)
        {
            if (value == null)
                value = nullValue;
            return value.Value;
        }
        private int GetValue(int? value, int nullValue)
        {
            if (value == null)
                value = nullValue;
            return value.Value;
        }
        private double GetValue(double? value, double nullValue)
        {
            if (value == null)
                value = nullValue;
            return value.Value;
        }
        private void LogToFile(int curvePrepDistance, double idealBankingSpeed)
        {
            if (carPositionsTicks.Count > 0)
            {
                if (outputToConsole)
                    Console.WriteLine(carPositionsTicks[carPositionsTicks.Count - 1].ToString() +
                         "\t" + carPositions[carPositions.Count - 1].piecePosition.pieceIndex.ToString() +
                        "\tlap: " + carPositions[carPositions.Count - 1].piecePosition.lap +
                        "\tSpeed estimate: " + Math.Round(currVelocity, 3)
                        + " units/game tick" + "\tSlip angle: "
                        + Math.Round(carPositions[carPositions.Count - 1].angle, 3) + "\tState: "
                        + currThrottleState);
                if (!CI)
                {
                    string logfile = carName.Replace(" ","") + "bullrun_" + fileTime + ".csv";
                    bool writeHeader = true;
                    if (File.Exists(logfile))
                        writeHeader = false;
                    using (TextWriter tw = new StreamWriter(logfile, true))
                    {
                        string mcs = "0";
                        string mcsmod = "0";
                        if (curvePieceStats.ContainsKey(currPieceIndex))
                        {
                            mcs = curvePieceStats[currPieceIndex].maxCorneringSpeed.ToString();
                            mcsmod = curvePieceStats[currPieceIndex].corneringSpeedModifier.ToString();
                        }
                        if (writeHeader)
                            tw.WriteLine("Tick,Lap,LaneStartIdx,LaneEndIdx,startLaneDistFromCenter,endLaneDistFromCenter,Lane Adjustment,Piece idx,In Piece Distance,Piece length,Piece radius,Piece angle, Dist to Next Piece,dist to next curve,index to next curve,Next curve angle,Next curve radius,ThrottleToApply,Slip angle,is slipping,lastspeed estimate,acceleration estimate, ideal speed, curve prep distance, state, lastTargetVelocity, coefFriction, maxcorneringspeed, maxspeedMod");
                        tw.WriteLine(carPositionsTicks[carPositionsTicks.Count - 1].ToString() +
                                                    "," + carPositions[carPositions.Count - 1].piecePosition.lap.ToString() +
                                                    "," + carPositions[carPositions.Count - 1].piecePosition.lane.startLaneIndex.ToString() +
                                                   "," + carPositions[carPositions.Count - 1].piecePosition.lane.endLaneIndex.ToString() +
                                                    "," + race.track.lanes[carPositions[carPositions.Count - 1].piecePosition.lane.startLaneIndex].distanceFromCenter +
                                                   "," + race.track.lanes[carPositions[carPositions.Count - 1].piecePosition.lane.endLaneIndex].distanceFromCenter +
                                                   "," + currLaneAdjustmentDist.ToString() +
                                                   "," + currPieceIndex.ToString() +
                                                   "," + carPositions[carPositions.Count - 1].piecePosition.inPieceDistance.ToString() +
                                                   "," + currPieceLength.ToString() +
                                                   "," + currPieceRadius.ToString() +
                                                   "," + CurrPieceAngle.ToString() +
                                                   "," + distanceToNextPiece.ToString() +
                                                   "," + distanceToNextCurve.ToString() +
                                                   "," + nextCurveIndex.ToString() +
                                                   "," + GetValue(nextCurve.angle, 0).ToString() +
                                                   "," + GetValue(nextCurve.radius, 0).ToString() +
                                                   "," + currThrottle.ToString() +
                                                   "," + carPositions[carPositions.Count - 1].angle.ToString() +
                                                   "," + degOfSlip.ToString() +
                                                   "," + currVelocity.ToString() +
                                                   "," + currAcceleration.ToString() +
                                                   "," + idealBankingSpeed.ToString() +
                                                   "," + curvePrepDistance.ToString() +
                                                   "," + currThrottleState.ToString() +
                                                   "," + lastTargetVelocity.ToString() +
                                                   "," + coefFriction.ToString() +
                                                   "," + mcs +
                                                   "," + mcsmod
                                                   );
                    }
                }
            }
            dumpCurvePieceStats();
        }
        public void dumpAccelerationStats()
        {
            if (!CI)
            {
                accelerationTable2.dumpToFile(carName.Replace(" ","") + "accelerations" + fileTime + ".csv");
            }
        }
        public void dumpDecelerationsStats()
        {
            if (!CI)
            {
                decelerationTable2.dumpToFile(carName.Replace(" ","") + "decelerations" + fileTime + ".csv");
            }
        }
        public void dumpThrottleStats()
        {
            if (!CI)
            {
                string filename = carName.Replace(" ","")+"throttledump" + fileTime + ".csv";
                using (TextWriter tw = new StreamWriter(filename))
                {
                    tw.WriteLine(string.Join(",", new string[] {
                        "startingSpeed",
                        "throttleApplied",
                        "endingSpeed", 
                        "distanceTraveled",
                        "speedDiff",
                        "ticksElapsed",
                        "info",
                        "pieceAngle",
                        "pieceRadius"
                }));
                    foreach (ThrottleStatistic ts in throttleStats)
                    {
                        tw.WriteLine(string.Join(",", new string[] {
                        ts.startingSpeed.ToString(),
                        ts.throttleApplied.ToString(),
                        ts.endingSpeed.ToString(), 
                        ts.distanceTraveled.ToString(),
                        ts.speedDiff.ToString(),
                        ts.ticksElapsed.ToString(),
                        ts.info.ToString(),
                        ts.pieceAngle.ToString(),
                        ts.pieceRadius.ToString()
                    }));
                    }
                }
            }
        }
        public void dumpCurvePieceStats()
        {
            if (!CI)
            {
                string filename = carName.Replace(" ","") +"curvepiecedump" + fileTime + ".csv";
                bool writeheader = false;
                if (!File.Exists(filename))
                    writeheader = true;
                using (TextWriter tw = new StreamWriter(filename,true))
                {
                    if (writeheader)
                    {
                        tw.WriteLine(string.Join(",", new string[] {
                        "gameTick",
                        "Piece idx",
                        "Angle",
                        "CorneringSpeedModifier", 
                        "DistanceTo",
                        "MaxCorneringSpeed",
                        "Radius", 
                        "Coef_Friction" }));
                    }
                    foreach (KeyValuePair<int, CurvePieceStat> kv in curvePieceStats)
                    {
                        tw.WriteLine(string.Join(",", new string[] {
                        gameTick.ToString(),
                        kv.Key.ToString(),
                        kv.Value.angle.ToString(),
                        kv.Value.corneringSpeedModifier.ToString(),
                        kv.Value.distanceTo.ToString(), 
                        kv.Value.maxCorneringSpeed.ToString(),
                        kv.Value.radius.ToString(),
                        coefFriction.ToString()
                    }));
                    }
                }
            }
        }
        public void dumpPieceStats()
        {
            if (!CI)
            {
                string filename = carName.Replace(" ","")+"piecedump" + fileTime + ".csv";
                using (TextWriter tw = new StreamWriter(filename))
                {
                    tw.WriteLine(string.Join(",", new string[] {
                        "Piece idx",
                        "MaxSpeed",
                        "MinSpeed", 
                        "MaxSlipAngle",
                        "MinSlipAngle"
                }));
                    foreach (KeyValuePair<int, PieceStatistic> kv in pieceStats)
                    {
                        tw.WriteLine(string.Join(",", new string[] {
                        kv.Key.ToString(),
                        kv.Value.maxVelocity.ToString(),
                        kv.Value.minVelocity.ToString(),
                        kv.Value.maxSlipAngle.ToString(), 
                        kv.Value.minSlipAngle.ToString()

                    }));
                    }
                }
            }
        }
    }

    class CurvePieceStat
    {
        public double corneringSpeedModifier = 0;
        public double distanceTo = 0;
        public double maxCorneringSpeed = 0;
        public double radius = 0;
        public double angle = 0;
        public double currentBreakingDistance = 0;
        public CurvePieceStat(double Angle, double Radius)
        {
            angle = Angle;
            radius = Radius;
        }
    }

    class AccelerationTable2
    {
        private SortedList accelerations = new SortedList();
        public bool isReady { get { return accelerations.Count > 15; } }
        public AccelerationTable2()
        {

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="velocity">velocity in game units/tick</param>
        /// <param name="deceleration">deceleration rate in game units/tick</param>
        public void Add(double velocity, double acceleration)
        {

            velocity = Math.Round(velocity, 4);
            if (!accelerations.ContainsKey(velocity))
                accelerations.Add(velocity, acceleration);

            /*for (int i = 0; i< decelerations.Count; i++)
            {

            }*/
        }

        public double GetSpeedUpDistance(double startingVelocity, double EndingVelocity)
        {
            if (EndingVelocity <= startingVelocity)
                return 0;
            double distance = 0;
            while (startingVelocity < EndingVelocity)
            {
                double addup = BinSearchA_getValue(startingVelocity);
                startingVelocity += addup;
                if (startingVelocity == -1)
                    return -1; //prevent infinite looping
                distance += startingVelocity;
                if (startingVelocity >= EndingVelocity)
                    return distance;
            }
            return -1;
        }

        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private double BinSearchA_getValue(double searchValue)
        {
            int imin = 0;

            int imax = accelerations.Count - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                double midValue = (double)accelerations.GetKey(imid);
                if (midValue == searchValue)
                    return (double)accelerations.GetByIndex(imid);
                else if (Math.Abs(imax - imin) <= 1)
                    return (double)accelerations.GetByIndex(imax);
                else if (midValue < searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }

        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in Ascending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private int BinSearchA(double searchValue)
        {
            int imin = 0;

            int imax = accelerations.Count - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                double midValue = (double)accelerations.GetKey(imid);
                if (midValue == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) <= 1)
                    return imax;
                else if (midValue < searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }

        public void dumpToFile(string filename)
        {


            using (TextWriter tw = new StreamWriter(filename, true))
            {
                tw.WriteLine("velocity,acceleration");

                for (int i = 0; i < accelerations.Count; i++)
                {
                    tw.WriteLine(accelerations.GetKey(i).ToString() + "," + accelerations.GetByIndex(i).ToString());
                }
            }
        }

    }

    class DecelerationTable2
    {
        private SortedList decelerations = new SortedList();
        public bool isReady { get { return decelerations.Count>15 ; } } 
        public DecelerationTable2()
        {

        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="velocity">velocity in game units/tick</param>
        /// <param name="deceleration">deceleration rate in game units/tick</param>
        public void Add(double velocity, double deceleration)
        {
            velocity = Math.Round(velocity, 4);
            if (deceleration > .03 && deceleration < .5)//outside of this range would be erroneous
            {
                if (!decelerations.ContainsKey(velocity))
                        decelerations.Add(velocity, deceleration);
            }
        }

        public double GetBreakingDistance(double startingVelocity, double EndingVelocity)
        {
            if (EndingVelocity >= startingVelocity)
                return 0;
            double distance = 0;
            while (startingVelocity > EndingVelocity)
            {
                double drop = BinSearchA_getValue(startingVelocity);
                startingVelocity -= drop;
                if (startingVelocity == -1)
                    return -1; //prevent infinite looping
                distance += startingVelocity;
                if (startingVelocity <= EndingVelocity)
                    return distance;
            }
            return -1;
        }

        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private double BinSearchA_getValue(double searchValue)
        {
            int imin = 0;

            int imax = decelerations.Count - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                double midValue = (double)decelerations.GetKey(imid);
                if (midValue == searchValue)
                    return (double)decelerations.GetByIndex(imid);
                else if (Math.Abs(imax - imin) <= 1)
                    return (double)decelerations.GetByIndex(imax);
                else if (midValue < searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }

        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private int BinSearchA(double searchValue)
        {
            int imin = 0;
           
            int imax = decelerations.Count -1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                double midValue = (double)decelerations.GetKey(imid);
                if (midValue == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) <= 1)
                    return imax;
                else if (midValue < searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }

        public void dumpToFile(string filename)
        {


            using (TextWriter tw = new StreamWriter(filename, true))
            {
                tw.WriteLine("velocity,deceleration");

                for (int i = 0; i<decelerations.Count; i++)
                { 
                    tw.WriteLine(decelerations.GetKey(i).ToString() + "," + decelerations.GetByIndex(i).ToString());
                }
            }
        }

    }

    class DecelerationTable
    {
        private double[] decelerations;
        private double[] velocities;
        //private int[] relativeDecayTicks;
        private int relativeStartTick;
        private const int numTickDecelerations = 200;
        public DecelerationTable()
        {
            relativeStartTick = -50;
            decelerations = new double[numTickDecelerations];
            velocities = new double[numTickDecelerations];
            //relativeDecayTicks = new int[NumTickDecelerations];
            Populate(8.336549252, 4); //numbers from throttledum.xlsx (sheet4) //population of velocities will be most accurate around the initvelocity
        }

        /// <summary>
        /// Populates table using exponential curve formula from test data.
        /// initial velocity allow for velocity lookups into table.  Error increased as we get away from initial velocity.
        /// </summary>
        private void Populate(double initVelocity, int initTick)
        {
            int relativeTick = relativeStartTick;
            int initTixIdx = -1;
            for (int i = 0; i < decelerations.Length; i++)
            {
                if (initTick == relativeTick)
                    initTixIdx = i;
                double decelerationValue = 0.1845 * (Math.Pow(2.718281828, -.02 * relativeTick));
                decelerations[i] = decelerationValue;
                relativeTick++;
            }
            velocities[initTixIdx] = initVelocity;
            for (int i = initTixIdx + 1; i < decelerations.Length; i++)
                velocities[i] = velocities[i - 1] - decelerations[i];
            for (int i = initTixIdx - 1; i >= 0; i--)
                velocities[i] = velocities[i + 1] + decelerations[i];
        }
        public double GetBreakingDistance(double startingVelocity, double EndingVelocity)
        {
            if (EndingVelocity >= startingVelocity)
                return 0;
            //deceleration = Math.Abs(deceleration);
            int idx = BinSearchD(velocities, startingVelocity);
            if (idx != -1)
            {
                double distance = 0;
                for (int i = idx; i < decelerations.Length - 1; i++)
                {
                    startingVelocity -= decelerations[i];
                    distance += startingVelocity;
                    if (startingVelocity <= EndingVelocity)
                        return distance;
                }
            }
            return -1;
        }
        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private static int BinSearchD(double[] descSortedArray, double searchValue)
        {
            int imin = 0;
            int imax = descSortedArray.Length - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                if (descSortedArray[imid] == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) <= 1)
                    return imax;
                else if (descSortedArray[imid] > searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }
        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        private static int BinSearchD(int[] descSortedArray, int searchValue)
        {
            int imin = 0;
            int imax = descSortedArray.Length - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                if (descSortedArray[imid] == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) <= 1)
                    return imax;
                else if (descSortedArray[imid] > searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }
    }
    class PieceStatistic
    {
        private List<double> slipAngles = new List<double>();
        private List<double> velocities = new List<double>();

        public PieceStatistic(double Velocity, double SlipAngle)
        {
            AddVelocity(Velocity);
            AddSlipAngle(SlipAngle);

        }

        private void AddVelocity(double Velocity)
        {
            velocities.Add(Velocity);
            if (Math.Abs(Velocity) > Math.Abs(maxVelocity))
                maxVelocity = Velocity;
            if (Math.Abs(Velocity) < Math.Abs(minVelocity))
                minVelocity = Velocity;
        }
        private void AddSlipAngle(double SlipAngle)
        {
            slipAngles.Add(SlipAngle);
            if (Math.Abs(SlipAngle) > Math.Abs(maxSlipAngle))
                maxSlipAngle = SlipAngle;
            if (Math.Abs(SlipAngle) < Math.Abs(minSlipAngle))
                minSlipAngle = SlipAngle;
        }

        public void Update(double Velocity, double SlipAngle)
        {
            AddVelocity(Velocity);
            AddSlipAngle(SlipAngle);
        }
        //private totalVelocity = 0;
        public double maxSlipAngle;
        public double minSlipAngle;
        public double maxVelocity;
        public double minVelocity;
        //public double averageVelocity; 
        //public bool endOfCurve;
    }
    class ThrottleStatistic
    {
        public double startingSpeed;
        public double throttleApplied;
        public string info;
        public double endingSpeed;
        public int ticksElapsed;
        public double distanceTraveled;
        public double speedDiff;
        public double pieceAngle;
        public double pieceRadius;

        public ThrottleStatistic(double StartingSpeed, double ThrottleApplied, double EndingSpeed, int TicksElapsed, double DistanceTraveled, string Info, double PieceAngle, double PieceRadius)
        {
            startingSpeed = StartingSpeed;
            throttleApplied = ThrottleApplied;
            endingSpeed = EndingSpeed;
            ticksElapsed = TicksElapsed;
            distanceTraveled = DistanceTraveled;
            info = Info;
            speedDiff = endingSpeed - startingSpeed;

            pieceAngle = PieceAngle;
            pieceRadius = PieceRadius;
        }

    }
    class Utility
    {
        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        public static int BinSearchD(double[] descSortedArray, double searchValue)
        {
            int imin = 0;
            int imax = descSortedArray.Length - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                if (descSortedArray[imid] == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) == 1)
                    return imax;
                else if (descSortedArray[imid] > searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }
        /// <summary>
        /// Searches an array of doubles for the index nearest to search value
        /// </summary>
        /// <param name="descSortedArray">An array of doubles sorted in descending order</param>
        /// <param name="searchValue">The value to find</param>
        /// <returns>index of found item. If an exact match is not found, returns largest of two nearest terms. returns -1 if nothing is found.</returns>
        public static int BinSearchA(double[] AscSortedArray, double searchValue)
        {
            int imin = 0;
            int imax = AscSortedArray.Length - 1;
            while (imax >= imin)
            {
                int imid = imin + ((imax - imin) / 2);
                if (AscSortedArray[imid] == searchValue)
                    return imid;
                else if (Math.Abs(imax - imin) == 1)
                    return imax;
                else if (AscSortedArray[imid] < searchValue)  //flip equality to search array sorted in opposite direction
                    imin = imid + 1;
                else
                    imax = imid - 1;
            }
            return -1;
        }
    }
}
